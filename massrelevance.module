<?php

/**
 * @file
 * Module implementation file.
 */

/**
 * Implements hook_field_info().
 */
function massrelevance_field_info() {
  return array(
    'massrelevance' => array(
      'label' => t('Mass Relevance widget'),
      'description' => t('Generates a Mass Relevance widget.'),
      'instance_settings' => array('widget_type' => 'button_poll'),
      'default_widget' => 'massrelevance_widget',
      'default_formatter' => 'massrelevance_formatter_iframe',
    ),
  );
}

/**
 * Implements hook_field_widget_info().
 */
function massrelevance_field_widget_info() {
  return array(
    'massrelevance_widget' => array(
      'label' => t('Mass relevance'),
      'field types' => array('massrelevance'),
      'settings' => array(),
    ),
  );
}

/**
 * Implememnts hook_field_formatter_info().
 */
function massrelevance_field_formatter_info() {
  // The three output formats Mass Relevance currently supports.
  return array(
    'massrelevance_formatter_iframe' => array(
      'label' => t('Mass relevance iframe'),
      'description' => t('Output your widget using an iframe.'),
      'field types' => array('massrelevance'),
      'settings' => array(
        'width' => '625',
        'height' => '400',
      ),
    ),
    'massrelevance_formatter_facebook' => array(
      'label' => t('Mass relevance Facebook'),
      'description' => t('Output your widget using an iframe compatible with Facebook.'),
      'field types' => array('massrelevance'),
      'settings' => array(
        'width' => '625',
        'height' => '400',
      ),
    ),
    'massrelevance_formatter_html5' => array(
      'label' => t('Mass relevance HTML5'),
      'description' => t('Output your widget using HTML5.'),
      'field types' => array('massrelevance'),
    ),
  );
}

/**
 * Implements hook_field_formatter_settings_form().
 */
function massrelevance_field_formatter_settings_form($field, $instance, $view_mode, $form, &$form_state) {
  $display = $instance['display'][$view_mode];
  $settings = $display['settings'];

  $element = array();

  if ($display['type'] == 'massrelevance_formatter_iframe' || $display['type'] == 'massrelevance_formatter_facebook') {
    $element['width'] = array(
      '#title' => t('Width'),
      '#desctiption' => t('Width of the widget in pixels.'),
      '#type' => 'textfield',
      '#size' => 8,
      '#default_value' => $settings['width'],
      '#element_validate' => array('element_validate_integer_positive'),
      '#required' => TRUE,
    );
    $element['height'] = array(
      '#title' => t('Height'),
      '#desctiption' => t('Height of the widget in pixels.'),
      '#type' => 'textfield',
      '#size' => 8,
      '#default_value' => $settings['height'],
      '#element_validate' => array('element_validate_integer_positive'),
      '#required' => TRUE,
    );
  }
  return $element;
}

/**
 * Implement hook_field_formatter_settings_summary().
 */
function massrelevance_field_formatter_settings_summary($field, $instance, $view_mode) {
  $display = $instance['display'][$view_mode];
  $settings = $display['settings'];

  $summary = '';

  if ($display['type'] == 'massrelevance_formatter_iframe' || $display['type'] == 'massrelevance_formatter_facebook') {
    $summary = t('Width: @width px. Height: @height px.', array(
      '@width' => $settings['width'],
      '@height' => $settings['height'],
    ));
  }
  return $summary;
}

/**
 * Implements hook_field_validate().
 */
function massrelevance_field_validate($entity_type, $entity, $field, $instance, $langcode, $items, &$errors) {
  foreach ($items as $delta => $item) {
    if (!empty($item['widget_code'])) {
      if (! preg_match('@/@', $item['widget_code'])) {
        $errors[$field['field_name']][$langcode][$delta][] = array(
          'error' => 'massrelevance_invalid',
          'message' => t('You need to input the widget code like <em>username/widget-id</em>.'),
        );
      }
    }
  }
}

/**
 * Implements hook_field_is_empty().
 */
function massrelevance_field_is_empty($item, $field) {
  return empty($item['value']);
}

/**
 * Implements hook_field_formatter_view().
 */
function massrelevance_field_formatter_view($entity_type, $entity, $field, $instance, $langcode, $items, $display) {
  $element = array();
  $widget_type = !empty($instance['widget']['settings']['widget_type']) ? $instance['widget']['settings']['widget_type'] : $instance['settings']['widget_type'];

  // Set the common info to the element array.
  $element_base = array(
    '#theme' => 'massrelevance_widget',
    '#widget_type' => $widget_type,
    '#display_type' => 'iframe',
    '#width' => $display['settings']['width'],
    '#height' => $display['settings']['height'],
  );
  // Set the widget type.
  switch ($display['type']) {
    case 'massrelevance_formatter_iframe':
      break;
    case 'massrelevance_formatter_facebook':
      $element_base['#display_type'] = 'facebook';
      break;
    case 'massrelevance_formatter_HTML5':
      $element_base['#display_type'] = 'html5';
      break;
  }
  foreach ($items as $delta => $item) {
    $element[$delta] = $element_base;
    // Set the widget id to render.
    $element[$delta]['#widget_code'] = $item['value'];
  }

  return $element;
}

/**
 * Implements hook_field_widget_form().
 */
function massrelevance_field_widget_form(&$form, &$form_state, $field, $instance, $langcode, $items, $delta, $element) {
  $value = isset($items[$delta]['value']) ? $items[$delta]['value'] : '';

  $widget = $element;
  $widget['#delta'] = $delta;

  switch ($instance['widget']['type']) {
    case 'massrelevance_widget':
      $widget += array(
        '#type' => 'textfield',
        '#default_value' => $value,
      );
      break;
  }

  $element['value'] = $widget;
  return $element;
}

/**
 * Implements hook_field_widget_settings_form().
 */
function massrelevance_field_widget_settings_form($field, $instance) {
  $widget = $instance['widget'];
  $settings = $widget['settings'];
  switch ($widget['type']) {
    case 'massrelevance_widget':
      $options = array();
      foreach (massrelevance_massrelevance_widget_info() as $key => $info) {
        $options[$key] = $info['label'];
      }
      $form['widget_type'] = array(
        '#type' => 'select',
        '#title' => t('Type of widget.'),
        '#default_value' => !empty($settings['widget_type']) ? $settings['widget_type'] : $instance['settings']['widget_type'],
        '#options' => $options,
        '#required' => TRUE,
      );
      break;
  }
  return $form;
}

/**
 * Implements hook_field_widget_error().
 */
function massrelevance_field_widget_error($element, $error, $form, &$form_state) {
  switch ($error['error']) {
    case 'massrelevance_invalid':
      form_error($element, $error['message']);
      break;
  }
}

/**
 * Implements hook_theme().
 */
function massrelevance_theme($existing, $type, $theme, $path) {
  return array(
    'massrelevance_widget' => array(
      'variables' => array(
        'widget_code' => NULL,
        'widget_type' => NULL,
        'display_type' => NULL,
        'width' => NULL,
        'height' => NULL,
      ),
      'file' => 'massrelevance.theme.inc'
    ),
  );
}

/**
 * Provides all the necessary information about the available widget types.
 */
function massrelevance_massrelevance_widget_info() {
  return array(
    'single_stream' => array(
      'label' => t('Single Stream'),
      'component' => 'mass-streams',
      'version' => '11',
    ),
    'multi_stream' => array(
      'label' => t('Multi-Stream'),
      'component' => 'mass-streams',
      'version' => '12',
    ),
    'spotlight_stream' => array(
      'label' => t('Spotlight + Stream'),
      'component' => 'spotlight-stream',
      'version' => '8',
    ),
    'media_wall' => array(
      'label' => t('Media Wall'),
      'component' => 'photo-wall',
      'version' => 'v10',
    ),
    'tug_o_war' => array(
      'label' => t('Tug-o-War'),
      'component' => 'tug-o-war',
      'version' => 'v7',
    ),
    'button_poll' => array(
      'label' => t('Button Poll'),
      'component' => 'button-poll',
      'version' => '4',
    ),
    'top_hashtags' => array(
      'label' => t('Top Hashtags'),
      'component' => 'leaderboard',
      'version' => '7',
    ),
    'list_poll' => array(
      'label' => t('List Poll'),
      'component' => 'bullet-poll',
      'version' => '8',
    ),
//    'spotlight' => array(
//      'label' => t('Spotlight'),
//      'component' => '',
//      'version' => '',
//    ),
//    'photo_spotlight' => array(
//      'label' => t('Photo Spotlight'),
//      'component' => '',
//      'version' => '',
//    ),
//    'avatar_wall' => array(
//      'label' => t('Avatar Wall'),
//      'component' => '',
//      'version' => '',
//    ),
//    'image_poll' => array(
//      'label' => t('Image Poll'),
//      'component' => '',
//      'version' => '',
//    ),
//    'video_poll' => array(
//      'label' => t('Video Poll'),
//      'component' => '',
//      'version' => '',
//    ),
//    'topic_list' => array(
//      'label' => t('Topic List'),
//      'component' => '',
//      'version' => '',
//    ),
//    'top_influencers' => array(
//      'label' => t('Top Influencers'),
//      'component' => '',
//      'version' => '',
//    ),
//    'top_tweets' => array(
//      'label' => t('Top Tweets'),
//      'component' => '',
//      'version' => '',
//    ),
//    'top_links' => array(
//      'label' => t('Top Links'),
//      'component' => '',
//      'version' => '',
//    ),
//    'image_grid' => array(
//      'label' => t('Image Grid'),
//      'component' => '',
//      'version' => '',
//    ),
//    'q_a' => array(
//      'label' => t('Q & A'),
//      'component' => '',
//      'version' => '',
//    ),
//    'q_a_stream' => array(
//      'label' => t('Q & A + Stream'),
//      'component' => '',
//      'version' => '',
//    ),
//    'video_q_a_stream' => array(
//      'label' => t('Video Q & A + Stream'),
//      'component' => '',
//      'version' => '',
//    ),
//    'fill_blank' => array(
//      'label' => t('Fill in the Blank'),
//      'component' => '',
//      'version' => '',
//    ),
//    'mad_lib' => array(
//      'label' => t('Mad Lib'),
//      'component' => '',
//      'version' => '',
//    ),
//    'numerical_counter' => array(
//      'label' => t('Numerical Counter'),
//      'component' => '',
//      'version' => '',
//    ),
//    'horizontal_progress_bar' => array(
//      'label' => t('Horizontal Progress Bar'),
//      'component' => '',
//      'version' => '',
//    ),
//    'vertical_progress_bar' => array(
//      'label' => t('Vertical Progress Bar'),
//      'component' => '',
//      'version' => '',
//    ),
//    'activity_map' => array(
//      'label' => t('Activity Map'),
//      'component' => '',
//      'version' => '',
//    ),
//    'heat_map' => array(
//      'label' => t('Heat Map'),
//      'component' => '',
//      'version' => '',
//    ),
//    'galaxy' => array(
//      'label' => t('Galaxy'),
//      'component' => '',
//      'version' => '',
//    ),
//    'bar_graph' => array(
//      'label' => t('Bar Graph'),
//      'component' => '',
//      'version' => '',
//    ),
//    'line_graph' => array(
//      'label' => t('Line Graph'),
//      'component' => '',
//      'version' => '',
//    ),
  );
}
